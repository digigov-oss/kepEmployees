import { isKepEmployee, getKepEmployeesAll } from "../dist/esm/index.js";
import config from "./config.json";

const test = async () => {
  try {
    const employee = await isKepEmployee("063823241", config.user, config.pass);
    return employee;
  } catch (error) {
    console.log(error);
  }
};

const testAll = async () => {
  try {
    const result = await getKepEmployeesAll(config.user, config.pass);
    return result;
  } catch (error) {
    console.log(error);
  }
};

test().then((response) => {
  console.log("isKepEmployee", response);
});

testAll().then((response) => {
  console.log("getKepEmployeesAll", response);
});
