import isKepEmployee from "../src/index";
import config from "./config.json";
import { PostgreSqlEngine } from "@digigov-oss/gsis-audit-record-db";
const test = async () => {
  try {
    const overides = {
      auditEngine: new PostgreSqlEngine(
        "postgres://postgres:postgres@localhost:5432/postgres"
      ),
      auditInit: {
        auditUnit: "grnet.gr",
      },
    };
    const employee = await isKepEmployee(
      "063823241",
      config.user,
      config.pass,
      overides
    );
    return employee;
  } catch (error) {
    console.log(error);
  }
};

test().then((employee) => {
  console.log("isKepEmployee", employee);
});
