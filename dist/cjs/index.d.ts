import { AuditRecord, AuditEngine } from "@digigov-oss/gsis-audit-record-db";
export declare type AuditInit = AuditRecord;
export declare type GateRecord = {
    gateDisplayName?: string;
    gateId?: string;
    afm?: string;
    role?: "clerk" | "head";
};
export declare type IsKepEmployeeOutputRecord = {
    data: {
        item: GateRecord[];
    };
};
export declare type GetKepEmployeesAllOutputRecord = {
    data: {
        item: GateRecord[];
    };
};
export declare type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 * Check if a person is a kep employee.
 *
 * @param afm the afm of the person
 * @param user string;
 * @param pass string;
 * @param overrides Overides;
 * @returns AuditRecord | errorRecord
 */
export declare const isKepEmployee: (afm: string, user: string, pass: string, overrides?: Overrides | undefined) => Promise<{
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    response: IsKepEmployeeOutputRecord | ErrorRecord;
}>;
export declare const getKepEmployeesAll: (user: string, pass: string, overrides?: Overrides | undefined) => Promise<{
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    response: GetKepEmployeesAllOutputRecord | ErrorRecord;
}>;
export default isKepEmployee;
