import soapClient from "./soapClient.js";
import { generateAuditRecord, FileEngine, } from "@digigov-oss/gsis-audit-record-db";
import config from "./config.json";
/**
 * Check if a person is a kep employee.
 *
 * @param afm the afm of the person
 * @param user string;
 * @param pass string;
 * @param overrides Overides;
 * @returns AuditRecord | errorRecord
 */
export const isKepEmployee = async (afm, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) {
        throw new Error("Audit record is not initialized");
    }
    try {
        const sClient = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const response = await sClient.isKepEmployee(afm);
        return {
            response: response,
            ...auditRecord,
        };
    }
    catch (error) {
        throw error;
    }
};
export const getKepEmployeesAll = async (user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) {
        throw new Error("Audit record is not initialized");
    }
    try {
        const sClient = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const response = await sClient.getKepEmployeesAll();
        return {
            response: response,
            ...auditRecord,
        };
    }
    catch (error) {
        throw error;
    }
};
export default isKepEmployee;
