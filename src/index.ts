import soapClient from "./soapClient.js";
import {
  generateAuditRecord,
  AuditRecord,
  FileEngine,
  AuditEngine,
} from "@digigov-oss/gsis-audit-record-db";
import config from "./config.json";

export type AuditInit = AuditRecord;

export type GateRecord = {
  gateDisplayName?: string;
  gateId?: string;
  afm?: string;
  role?: "clerk" | "head";
};

export type IsKepEmployeeOutputRecord = {
  data: {
    item: GateRecord[];
  };
};

export type GetKepEmployeesAllOutputRecord = {
  data: {
    item: GateRecord[];
  };
};

export type ErrorRecord = {
  errorCode: string;
  errorDescr: string;
};

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overrides = {
  endpoint?: string;
  prod?: boolean;
  auditInit?: AuditRecord;
  auditStoragePath?: string;
  auditEngine?: AuditEngine;
};

/**
 * Check if a person is a kep employee.
 *
 * @param afm the afm of the person
 * @param user string;
 * @param pass string;
 * @param overrides Overides;
 * @returns AuditRecord | errorRecord
 */
export const isKepEmployee = async (
  afm: string,
  user: string,
  pass: string,
  overrides?: Overrides
) => {
  const endpoint = overrides?.endpoint ?? "";
  const prod = overrides?.prod ?? false;
  const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
  const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
  const auditEngine =
    overrides?.auditEngine ?? new FileEngine(auditStoragePath);
  const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
  const auditRecord = await generateAuditRecord(auditInit, auditEngine);
  if (!auditRecord) {
    throw new Error("Audit record is not initialized");
  }

  try {
    const sClient = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response: IsKepEmployeeOutputRecord | ErrorRecord =
      await sClient.isKepEmployee(afm);
    return {
      response: response,
      ...auditRecord,
    };
  } catch (error) {
    throw error;
  }
};

export const getKepEmployeesAll = async (
  user: string,
  pass: string,
  overrides?: Overrides
) => {
  const endpoint = overrides?.endpoint ?? "";
  const prod = overrides?.prod ?? false;
  const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
  const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
  const auditEngine =
    overrides?.auditEngine ?? new FileEngine(auditStoragePath);
  const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
  const auditRecord = await generateAuditRecord(auditInit, auditEngine);
  if (!auditRecord) {
    throw new Error("Audit record is not initialized");
  }

  try {
    const sClient = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response: GetKepEmployeesAllOutputRecord | ErrorRecord =
      await sClient.getKepEmployeesAll();
    return {
      response: response,
      ...auditRecord,
    };
  } catch (error) {
    throw error;
  }
};

export default isKepEmployee;
