# Client for kepEmployees service of KED

Client to connect to kepEmployees service, useful for nextjs/nodejs projects.

#### Example:

```
import isKepEmployee from '@digigov-oss/kep-employees';

const test = async () => {
   const overrides = {
        prod:false,
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        auditStoragePath: '/auditStorage',
    }

    try {
        return await isKepEmployee("052704062", "username", "password", overrides);
    } catch (error) {
        console.log(error);
    }
}

test().then((result) => { console.log('isKepEmployee', result); });
```

Furthermore, you can use `overrides` to override the default storage engine. Package `@digigov-oss/auditrecord-postgresql-engine` contains an alternative engine. Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.
For your tests, you don't need to use the `overrides` mechanism, in that case, the default storage path will be used ie `/tmp`. Look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records that you can use on auditInit.

#### Returns

an object like the following:

```
{
    data: {
        item: [
            {
                gateDisplayName: 'ΚΕΠ ΔΗΜΟΥ ΟΙΧΑΛΙΑΣ, ΝΕΔΑ',
                gateId: 'ΚΕΠ/0578',
                afm: '063823241',
                role: 'clerk'
            },
            {
                gateDisplayName: 'ΚΕΠ ΔΗΜΟΥ ΟΙΧΑΛΙΑΣ, ΝΕΔΑ',
                gateId: 'ΚΕΠ/0578',
                afm: '063823241',
                role: 'head'
            }
        ],
    },
    auditUnit: 'gov.gr',
    auditTransactionId: '52',
    auditProtocol: '1/2022-02-11',
    auditTransactionDate: '2022-02-11T08:39:50Z',
    auditUserIp: '127.0.0.1',
    auditUserId: 'system'
}
```

or an error message like:

```
{
  errorCode: 'GEN_INVALID_DATA',
  errorDescr: 'Το πεδίο afm δεν είναι έγκυρος Αριθμός Φορολογικού Μητρώου',
  auditUnit: 'gov.gr',
  auditTransactionId: '52',
  auditProtocol: '1/2022-02-11',
  auditTransactionDate: '2022-02-11T08:39:50Z',
  auditUserIp: '127.0.0.1',
  auditUserId: 'system'
}
```

##### Notes

you have to ask KED for documentation to get more info about the output and error fields.

#### * known issues
KED advertises a wrong endpoint(!) for the service on production WSDL. So, you have to use (override) the endpoint: `https://ked.gsis.gr/esb/kepEmployeesService`
You can do that by setting the `endpoint` property on the `overrides` object.

```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/notificationCenterElementsService',
}
```
